var main, clickable;

function init(){

	console.log("INIT")

	//SET ELEMENT VARIABLES FOR ACCESS (NOT ALWAYS NEEDED FOR SIMPLE EXECUTIONS WHEN USING TweenMax)

	main = document.getElementById("viewport");
	clickable = document.getElementById("clickable");

	console.log(main);

	eventListeners();

  	trackClips();

}

function trackClips(){

	// SET ELEMENT INITIAL PROPERTY VALUES HERE

	TweenMax.set(border, {alpha:1})
	TweenMax.set([background, theSVG, beginning, logo], {alpha:1})
	TweenMax.set([middle, end], {alpha:0})

	TweenMax.to(shape13B, 1, {rotation:-40, y:1, delay:0.5, transformOrigin:"left bottom", ease:Power3.easeInOut, repeat:1, yoyo:true})
	TweenMax.to(shape14B, 1, {rotation:60, y:1, transformOrigin:"left top", ease:Power3.easeInOut, repeat:1, yoyo:true})

	TweenMax.to(shape24B, 1, {scaleY:1.7, ease:Power3.easeInOut, delay:0.5, repeat:1, yoyo:true})
	TweenMax.to(shape25B, 1, {scaleY:1.5, ease:Power3.easeInOut, transformOrigin:"center center", repeat:1, yoyo:true})


	setTimeout(function(){
		parseSVG('B', '', true);
		frameOne();
	}, 2000);
}

var beginningChildren;
var middleChildren;
var endChildren;

function parseSVG(currentStage, nextStage, t){
	middleChildren = document.getElementById("middle").children;

	for(var i=0; i<middleChildren.length; i++){
		// var r = randomNumber(0.8, 0.9);
		var r = 0.4;
		var d = i / 60;
		var shape = "#shape" + i;
		var currentShape = shape + currentStage;
		var nextShape = shape + nextStage;

		var tm = TweenMax.to(currentShape, 1, {morphSVG:nextShape, ease: Back.easeInOut.config(1.7), delay:d, onComplete:aniamteShape, onCompleteParams:[currentShape, t]})
		// console.log(tm);
		TweenMax.fromTo(tm, 1, {timeScale:0.5, rotation:0}, {timeScale:4, rotation:90, ease: Back.easeIn.config(1.7)})
	}
}

function randomNumber(min,max){
    return Math.floor(Math.random()*(max-min+1)+min);
}

function eventListeners(){

	console.log("Setting Buttons");

	//clickable.style.cursor = "pointer"
	//clickable.addEventListener("click", mainExit, false);
	//clickable.addEventListener("mouseover", bannerOver, false);
	//clickable.addEventListener("mouseout", bannerOut, false);

}

function back() {
	TweenMax.to(shape22B, 1, {rotation:0, transformOrigin:"left bottom", ease:Power1.easeInOut, delay:0.1})
}
// TweenMax.to(shape0B, 1.5, {rotation:13, transformOrigin:"left top", ease:Power3.easeInOut, delay:3.2, repeat:-1, yoyo:true})
// TweenMax.to(shape32B, 1.5, {y:-5, ease:Power3.easeInOut, delay:2.9, repeat:-1, yoyo:true})
function frameOne(){

	setTimeout(function(){
		parseSVG('B', 'E', false);
		frameTwo();
	}, 5000);
}

function frameTwo(){

	// explode();

	TweenMax.to(shape13B, 1, {rotation:40, y:1, transformOrigin:"left top", ease:Power3.easeInOut, delay:1.6, repeat:7, yoyo:true})

	TweenMax.to(shape14B, 1, {rotation:10, y:1, transformOrigin:"left top", ease:Power3.easeInOut, delay:1.3, repeat:7, yoyo:true})

	TweenMax.to(shape22B, 1, {scaleY:1.9, transformOrigin:"center center", ease:Power3.easeInOut, delay:1.3, repeat:7, yoyo:true})

		TweenMax.to(shape24B, 1, {scaleX:1.5, scaleY:1.5, transformOrigin:"center center", ease:Power3.easeInOut, delay:1.4, repeat:7, yoyo:true})
	var txt = document.querySelectorAll('.text');
	TweenMax.staggerTo(txt, 0.5, {delay:1.8, alpha:1}, 1.5)
	TweenMax.to(cta, 0.5, {delay:5.9, alpha:1, onComplete:bannerOver})
	TweenMax.set(ctaOver, {alpha:0})

	clickable.style.cursor = "pointer"
	clickable.addEventListener("mouseover", bannerOver, false);

}

function bannerOver(e){
	TweenMax.to(ctaOver, 0.35, {alpha:0.4, onComplete:bannerOut, overide:0})
	TweenMax.to(sheen, 0.7, {x:220})
}

function bannerOut(e){
	TweenMax.to(ctaOver, 0.35, {alpha:0, onComplete:setSheen})
}

function setSheen(){
	TweenMax.set(sheen, {x:0})
}

function aniamteShape(shape, t) {
	var r = 3;
	var dub = r * 2;
	var s = 2.3;

	if (t) {
		if (shape === '#shape1B') {
			TweenMax.to(shape6B, 0.5, {y:5, ease:Power3.easeInOut, repeat:dub, yoyo:true})
		}
		if (shape === '#shape2B') {
			TweenMax.to(shape5B, 0.5, {y:5, delay:0.1, ease:Power3.easeInOut, repeat:dub, yoyo:true})
		}
		if (shape === '#shape3B') {
			TweenMax.to(shape4B, 0.5, {y:5, delay:0.2, ease:Power3.easeInOut, repeat:dub, yoyo:true})
		}
		if (shape === '#shape4B') {
			TweenMax.to(shape3B, 0.5, {y:5, delay:0.3, ease:Power3.easeInOut, repeat:dub, yoyo:true})
		}
		if (shape === '#shape5B') {
			TweenMax.to(shape2B, 0.5, {y:5, delay:0.4, ease:Power3.easeInOut, repeat:dub, yoyo:true})
		}
		if (shape === '#shape6B') {
			TweenMax.to(shape1B, 0.5, {y:5, delay:0.5, ease:Power3.easeInOut, repeat:dub, yoyo:true})
		}
		if (shape === '#shape8B') {
			TweenMax.to(shape8B, 0.5, {rotation:-5, transformOrigin:"left bottom", ease:Power3.easeInOut, repeat:r, yoyo:true})
		}

		if (shape === '#shape12B') {
			TweenMax.to(shape12B, 1, {rotation:-180, x:2, transformOrigin:"right center", ease:Power3.easeInOut, repeat:r, yoyo:true})
		}

		if (shape === '#shape20B') {
			TweenMax.to(shape20B, 1, {rotation:5, transformOrigin:"right bottom", ease:Power3.easeInOut,  repeat:r, yoyo:true})
		}

		if (shape === '#shape23B') {
			TweenMax.to(shape23B, 1, {y:48, ease:Power3.easeInOut, repeat:r, yoyo:true})
		}

		if (shape === '#shape26B') {
				TweenMax.to(shape26B, 1, {y:-48, ease:Power3.easeInOut, repeat:r, yoyo:true})
		}

		if (shape === '#shape27B') {
			TweenMax.to(shape27B, 1, {rotation:-20, transformOrigin:"left bottom", ease:Power3.easeInOut, repeat:r, yoyo:true})
		}

		if (shape === '#shape38B') {
			TweenMax.to(shape38B, 1, {y:s, ease:Power3.easeInOut, repeat:r, yoyo:true})
		}

		if (shape === '#shape40B') {
			TweenMax.to(shape40B, 1, {rotation:90, y:1, transformOrigin:"left bottom", ease:Power3.easeInOut, repeat:r, yoyo:true})
		}
	}

}
